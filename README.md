#ToDo be slain!
###A ToDo App to help you stay organized utilizing BackboneJS, Less, Grunt and Jade!
##Getting Started
###To get started, make sure you have grunt, less and browser-sync installed globally:

```
    npm i g grunt-cli, jade, less, browser-sync
```
###Next change directories to inside the folder and install all of our dev dependencies from the package.json

```
    npm init
```
###The grunt config is setup with browser-sync to magically reload your webpage when any html or css file is edited, it will also compile your less to css just like magic, just run!:

```
    grunt
```

##Features
+ Checklist!
+ LocalStorage presistence (your stuff is always local)
+ Stats see how many tasks you've finished (you boss, you)

##Quirks:
* Sometimes web things don't update properly, if you find that your changes are having the right affect. Just do a **Hard** refresh:
    + Hold ctrl and press F5
* On some windows machines you'll need to install Visual Studio ****2013*** as some of the function for browser-sync need Visual C++ libraries. After you install it just reinstall the browser-sync grunt plugin this time specifying which VS you have:
    + ```
        npm i g browser-sync --msvs_version=2013
    ```



