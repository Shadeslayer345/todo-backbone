// =============================================================================
// Grunt Config ================================================================
// =============================================================================
// Wrapper function (required by grunt and its plugins)
// all configuration goes inside this function
module.exports = function(grunt) {

  // ===========================================================================
  // LOAD GRUNT PLUGINS ========================================================
  // ===========================================================================
  // Loaded if they are in our package.json.
  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

  // ===========================================================================
  // CONFIGURE GRUNT ===========================================================
  // ===========================================================================
  grunt.initConfig({
      // Get the configuration info from package.json
      pkg: grunt.file.readJSON('package.json'),

      watch: {
          files: ['app/src/less/*.less', 'app/src/jade/*.jade'],
          tasks: ['less', 'jade']
      },
      less: {
          dev: {
              files: {
                "app/dist/css/main.css" : "app/src/less/*.less"
              }
          }
      },
      jade: {
           options: {
              pretty: true
           },
           compile: {
             files: [{
               expand: true,
               cwd: "app/src/jade",
               src: "*.jade",
               dest: "app/dist/templates",
               ext: ".html"
             }]
           }
       },
      browserSync: {
          bsFiles: {
            src: ['app/dist/templates/*.html', 'app/index.html', 'app/dist/css/*.css']
          },
          options: {
              watchTask: true,
              server: {
                baseDir: "./app"
              },
              port: 9610
          }
      }
  });



  // ===========================================================================
  // CREATE TASKS ==============================================================
  // ===========================================================================
  grunt.registerTask('default', ['browserSync', 'watch']);
};
