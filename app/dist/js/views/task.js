define([
    'jquery',
    'underscore',
    'backbone',
    // Using a compiled jade template
    'text!dist/templates/task.html',
    'helper'
], function($, _, Backbone, taskListTemplate, Helper) {
    var TaskView = Backbone.View.extend({
        tagName: 'li',
        template: _.template(taskListTemplate),
        events: {
            'click .toggle':    'toggleCompleted',
            'dbclick label':    'edit',
            'click .remove':    'remove',
            'kepress .edit':    'updateOnEnter',
            'keydown .edit':    'undoOnEscape',
            'blur .edit':       'saveOnExit'
        },
        /**
         * Set's the view to always listen for changes to the model and execute
         *         the approprite function.
         */
        initialize: function() {
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'destroy', this.remove);
            this.listenTo(this.model, 'visible', this.toggleVisible);
        },
        /**
         * Creates the html that allows a model to be seen, updates the model.
         */
        render: function() {
            // Render the template in html and fill with data from model
            this.$el.html(this.template(this.model.toJSON()));
            this.$el.toggleClass('completed', this.model.get('completed'));
            // Take what was entered and set it as the input
            this.toggleVisible();
            this.$input = this.$('.edit');
            return this;
        },
        /**
         * Turns the visibility of the view on or off, based on isHidden.
         */
        toggleVisible: function() {
            this.$el.toggleClass('hidden', this.isHidden());
        },
        /**
         * Determines if an element is hidden based on it's completed status
         */
        isHidden: function() {
            var isComplete = this.model.get('completed');
            return (
                (!isComplete && Helper.TaskFilter === 'completed') ||
                (isComplete && Helper.TaskFilter === 'active')
            );
        },
        /**
         * Change tasks completions status to the opposite of its current state.
         */
        toggleCompleted: function() {
            this.model.toggle();
        },
        /**
         * Changes the view for modal to 'editing' (as defined by css).
         */
        edit: function() {
            this.$el.addClass('editing');
            this.$input.focus();
        },
        /**
         * Save on enter.
         * @param e the key event
         */
        updateOnEnter: function(e) {
            if (e.keyCode === Helper.ENTER_KEY) {
                this.saveOnExit();
            }
        },
        /**
         * Cancels on escape key.
         * @param e the key event
         */
        undoOnEscape: function(e) {
            if (e.which === HELPER.ESCAPE_KEY) {
                this.$el.removeClass('editing');
                this.$input.val(this.model.get('title'));
            }
        },
        /**
         * Saves new values and exits editing mode.
         * @return {[type]} [description]
         */
        saveOnExit: function() {
            var newVal = this.$input.val();
            var newTrimVal = newVal.trim();
            if (newTrimVal && newVal !== newTrimVal) {
                this.model.save({title: newTrimVal});
                this.model.trigger('change');
            } else {
                this.clear();
            }
            this.$el.removeClass('editing');
        },
        remove: function() {
            this.$el.addClass('hidden');
            this.model.destroy();
        }
    });

    return TaskView;
});
