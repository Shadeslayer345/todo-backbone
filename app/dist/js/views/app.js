define([
    'jquery',
    'underscore',
    'backbone',
    'dist/js/collections/task',
    'dist/js/views/task',
    'helper'
], function($, _, Backbone, TaskCollection, TaskView, Helper) {
    var AppView = Backbone.View.extend({
        el: '#todoapp',

        events: {
            'keypress #new-todo':       'createOnEnter',
            'click #clear-completed':   'clearCompleted',
            'click #toggle-all':        'toggleAllComplete'
        },

        initialize: function() {
            this.allCheckbox = this.$('#toggle-all')[0];
            this.$input = this.$('#new-todo');
            this.$footer = this.$('#footer');
            this.$main = this.$('#main');
            this.$taskList = this.$('#todo-list');

            this.listenTo(TaskCollection, 'add', this.addOne);
            this.listenTo(TaskCollection, 'reset', this.addAll);
            this.listenTo(TaskCollection, 'change:completed', this.filterOne);
            this.listenTo(TaskCollection, 'filter', this.filterAll);
            this.listenTo(TaskCollection, 'all', _.debounce(this.render, 0));

            TaskCollection.fetch({reset: true});
        },

        render: function() {
            var completed = TaskCollection.completed().length;
            var remaining = TaskCollection.remaining().length;

            if (TaskCollection.length) {
                this.$main.show();
                this.$footer.show();

                this.$('#filters li a')
                    .removeClass('selected')
                    .filter('[href="#/' + (Helper.TaskFilter || '') + '"]')
                    .addClass('selected');

            } else {
                this.$main.hide();
                this.$footer.hide();
            }

            this.allCheckbox.checked = !remaining;
        },

        filterOne: function(task) {
            task.trigger('visible');
        },

        filterAll: function() {
            TaskCollection.each(this.filterOne, this);
        },

        addOne: function(task) {
            var view = new TaskView({model: task});
            this.$taskList.append(view.render().el);
        },

        addAll: function() {
            this.$taskList.empty();
            TaskCollection.each(this.addOne, this);
        },

        newAttributes: function() {
            return {
                title: this.$input.val().trim(),
                order: TaskCollection.order(),
                completed: false
            };
        },

        createOnEnter: function(e) {
            if (e.which !== Helper.ENTER_KEY || !this.$input.val().trim()) {
                return;
            }

            TaskCollection.create(this.newAttributes());
            this.$input.val('');
        },

        clearCompleted: function() {
            _.invoke(TaskCollection.completed(), 'destroy');
            return false;
        },

        toggleAllComplete: function() {
            var completed = this.allCheckbox.checked;

            TaskCollection.each(function (task) {
                task.save({
                    completed: completed
                });
            });
        }
    });
    return AppView;
});
