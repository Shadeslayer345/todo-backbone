define([
    'underscore',
    'backbone',
    'localStorage',
    'dist/js/models/task'
], function(_, Backbone, Localstorage, TaskModel) {
    var TaskCollection = Backbone.Collection.extend({
        model: TaskModel,
        localStorage: new Localstorage("backbone-todo"),
        completed: function() {
            return this.where({completed: true});
        },
        remaining: function() {
            return this.where({completed: false});
        },
        order: function() {
            return this.length ? this.last().get('order') + 1 : 1;
        },
        comparator: 'order'
    });

    return new TaskCollection();
});
