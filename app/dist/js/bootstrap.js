// Entry point, defining our global dependencies and variables to be referenced and loaded by files

requirejs.config({
    baseUrl: "./",
    paths: {
        jquery: "src/js/libs/jquery-1.11.3.min",
        underscore: "src/js/libs/underscore-min",
        backbone: "src/js/libs/backbone-min",
        localStorage: "src/js/libs/backbone.localStorage-min",
        helper: "dist/js/helper",
        text: "src/js/libs/text"
    }
});

require([
    'dist/js/app',
], function(App) {
    App.initialize();
});
