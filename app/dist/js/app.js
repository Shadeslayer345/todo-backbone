// Main module for app

define([
    'jquery',
    'underscore',
    'backbone',
    'dist/js/views/app',
    'dist/js/router',
], function($, _, Backbone, AppView, Router) {
    var initialize = function() {
        var router = new Router();
        Backbone.history.start();
        var appView = new AppView();
    };

    return {
        initialize: initialize
    };
});
