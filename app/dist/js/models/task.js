define([
    'underscore',
    'backbone'
], function(_, Backbone) {
    var TaskModel = Backbone.Model.extend({
        defaults: {
            title:'',
            completed: false,
            assignee: ''
        },
        toggle: function() {
            this.save({
                completed: !this.get('completed')
            });
        },
        addAssignee: function(name) {
            this.save({
                assignee: name
            });
        }
    });
    return TaskModel;
});
