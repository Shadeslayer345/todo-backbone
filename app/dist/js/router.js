define([
    'jquery',
    'underscore',
    'backbone',
    'dist/js/collections/task',
    'helper'
], function($, _, Backbone, TaskCollection, Helper) {
    var AppRouter = Backbone.Router.extend({
        routes: {
            '*filter': 'setFilter'
        },

        setFilter: function(type) {
            Helper.TaskFilter = type || '';

            TaskCollection.trigger('filter');
        }
    });

    return AppRouter;
});
